This is the Debian GNU/Linux r-cran-quantmod package of quantmod.  The quantmod
package provides functions to create trading rules. It was written by Jeff Ryan
and Joshua Ulrich.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'quantmod' to
'r-cran-quantmod' to fit the pattern of CRAN (and non-CRAN)
packages for R.

Files: *
Copyright: 2007 - 2017  Jeff Ryan
Copyright: 2009 - 2017  Jeff Ryan and Joshua Ulrich
License: GPL-3

Files: debian/*
Copyright: 2017  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.

For reference, the upstream DESCRIPTION file is included below:

   Package: quantmod
   Type: Package
   Title: Quantitative Financial Modelling Framework
   Version: 0.4-10
   Date: 2017-06-19
   Authors@R: c(
     person(given=c("Jeffrey","A."), family="Ryan", role=c("aut","cph")),
     person(given=c("Joshua","M."), family="Ulrich", role=c("cre","aut"), email="josh.m.ulrich@gmail.com                                                                                       "),
     person(given="Wouter", family="Thielen", role="ctb")
     )
   Depends: xts(>= 0.9-0), zoo, TTR(>= 0.2), methods
   Imports: curl
   Suggests: DBI,RMySQL,RSQLite,timeSeries,XML,downloader,jsonlite(>= 1.1)
   Description: Specify, build, trade, and analyse quantitative financial trading strategies.
   LazyLoad: yes
   License: GPL-3
   URL: http://www.quantmod.com https://github.com/joshuaulrich/quantmod
   BugReports: https://github.com/joshuaulrich/quantmod/issues
   NeedsCompilation: yes
   Packaged: 2017-06-19 22:53:31 UTC; josh
   Author: Jeffrey A. Ryan [aut, cph],
     Joshua M. Ulrich [cre, aut],
     Wouter Thielen [ctb]
   Maintainer: Joshua M. Ulrich <josh.m.ulrich@gmail.com>
   Repository: CRAN
   Date/Publication: 2017-06-20 10:36:21 UTC
